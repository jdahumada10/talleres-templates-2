package estructuras;

public class MapNode {
	/**
     * Número entero que describe el identificador único del nodo (aeropuerto).
     */
    public final int id;

    /**
     * Número que describe la latitud de la posición del nodo (aeropuerto).
     */
    public final double lat;
    
    /**
     * Número que describe la longitud de la posición del nodo (aeropuerto).
     */
    public final double lng;
    
    /**
     * Nombre del aeropuerto al que corresponde este nodo.
     */
    public final String name;
    
    /**
     * Ciudad donde se encuentra ubicado el aeropuerto al que corresponde este nodo.
     */
    public final String city;
    
    /**
     * País donde se encuentra ubicado el aeropuerto al que corresponde este nodo.
     */
    public final String country;
    
    /**
     * Código IATA del aeropuerto al que corresponde este nodo.
     */
    public final String IATA;
    

    /**
     * Constructor principal de un nodo que pertecenece a un mapa.
     *
     * @param id Número de identificación único asignado al presente nodo.
     * @param latitude Número que establece la latitud geográfica del nodo en el mapa real.
     * @param longitude Número que establece la longitud geográfica del nodo en el mapa real.
     */
    public MapNode(int id, double lat, double lng, String name, String city, String country, String IATA)
    {
         this.id = id;
         this.lat = lat;
         this.lng = lng;
         this.name = name;
         this.city = city;
         this.country = country;
         this.IATA = IATA;
    }
    
    /**
     * Método toString
     */
    @Override
    public String toString() {
    	return "IATA: "+IATA+" - ID: "+id;
    }
}
